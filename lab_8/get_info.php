<html>

<body>
    <form action="add_user.php" method="post">

        Firstname: <input type="text" name="firstname" required><br>
        Lastname: <input type="text" name="lastname" required><br>
        E-mail: <input type="email" name="email" required><br>
        Password: <input type="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,8}"
            title="A password must be between 6 and 8 characters, and must contain at least one lower case letter, one upper case letter and one digit."
            required>
        <br>
        Registration date: <input type="date" name="reg_date" required><br>
        <input type="submit" value="Submit Data">

    </form>
</body>

</html>