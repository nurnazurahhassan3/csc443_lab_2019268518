<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php

        $monthDays = array (
        'Splorch' => 23,
        'Sploo' => 28,
        'Splat' => 2,
        'Splatt' => 3,
        'Spleen' => 44,
        'Splune' => 30,
        'Spling' => 61,
        'Slendo' => 61,
        'Sloctember' => 31,
        'Slictember' => 31,
        'Splanet' => 30,
        'TheRest' => 33
        ) ;

        #Question b.i
        echo "Shortest Days in month: " . min($monthDays);
        echo '</br>';

        #Question b.ii
        echo "Name of the shortest month: " . array_search(min($monthDays),$monthDays);
        echo '</br>';

        #Question b.iii
        echo "Total number of days in a year: ". array_sum($monthDays) ;
    ?>
</body>

</html>