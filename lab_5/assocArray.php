<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
        #QUestion 4

        #Question A
        $monthDays = array (
            'January' => 31,
            'Febuary' => 28,
            'March' => 31,
            'April' => 30,
            'May' => 31,
            'June' => 30,
            'July' => 31,
            'August' => 31,
            'September' => 30,
            'October' => 31,
            'November' => 30,
            'December' => 31
        );

         #Question B (foreach loop) 
        foreach($monthDays as $month => $days) {
             echo "Month= " . $month . ", Days in Month= " . $days;
             echo "<br>"; 
         }

         #Question C
         echo "</br> <b>This is months that have 30 days: </b> </br>";
         foreach($monthDays as $month => $days) {
            if($days==30){
            echo "Month= " . $month;
            echo "<br>"; };
        }
         

        

    ?>
</body>

</html>