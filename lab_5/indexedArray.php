<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php

        #Question 1A
        $month = array('January','Febuary','March','April','May','June', 'July','August','September','October','November','December');

        #Question 1B (Looping for echo)
        $mLenght = count($month);
        for ($x=0;$x<$mLenght;$x++){
            echo '$month'."[$x]: ". $month[$x]. "</br>"; 
        }


        #Question 2 (Sorting)
        echo "</br>";
        asort($month,SORT_REGULAR);

        #Question 3 (foreach loop) 
        foreach($month as $x => $x_value) {
            echo "Key=" . $x . ", Value=" . $x_value;
            echo "<br>"; 
        }

    ?>
</body>

</html>